<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class CountersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counters = \App\Counter::all(); 
        return view('counters.list_counters', compact('counters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
       $prints = \App\Printer::getPrint();
        return view('counters.create_counters', compact('prints'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $ip = \App\Printer::find($request->id_print);
        $getsnmp = \App\Counter::getCountersSnmp($ip->ip);
        $counters = new \App\Counter;
        $counters->id_print=$request->get('id_print');
        $counters->total_page=$request->get('total_page');
        $counters->date=$request->get('date');
        $counters->save();

        return redirect('/counters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $counters= \App\Counter::find($id);
        $prints = \App\Printer::getPrint();
        return view('counters.edit_counters',compact('counters','id','prints','counter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $counters= \App\Counter::find($id);
        $counters->id_print=$request->get('id_print');
        $counters->total_page=$request->get('total_page');
        $counters->date=$request->get('date');
        $counters->save();
        return redirect('counters');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {        
        $counters = \App\Counter::find($id);
        $counters->delete();
        return redirect('counters');
    }
    public function snmpCounters()
    {        
        $lastDay = Carbon::now()->lastOfMonth()->toDateString();        
        $rulesactive = \App\Rule::getRulesActive($lastDay);
        $date = date('Y-m-d H:i');
        foreach($rulesactive->prints as $print){       
            $getsnmp = \App\Counter::getCountersSnmp($print->ip);
            $getCounter = \App\Counter::where('id_print',$print->id)->orderBy('id','desc')->first()->total_page;
            $pageCounter = ($getsnmp - $getCounter);
            if($pageCounter > 0){                
                $counters = new \App\Counter;           
                $counters->id_print=$print->id; 
                $counters->counters=$pageCounter;
                $counters->total_page=$getsnmp;
                $counters->date=$date;
                $counters->save();
            }
        }       
    }
    public function reportPrint()
    {
        $lastDay = Carbon::now()->lastOfMonth()->toDateString();        
        $rulesactive = \App\Rule::getRulesActive($lastDay);
        foreach($rulesactive->prints as $print){
        $getCounter = \App\Counter::where('id_print',$print->id)->orderBy('id','desc')->first()->total_page;
        $counter['counter'][] = $getCounter;
        $model['model'][] = $print->model;
        $data = [$counter,$model];
        }

        return response()->json($data);

    }
}
