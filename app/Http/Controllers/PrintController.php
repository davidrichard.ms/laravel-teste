<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $prints=\App\Printer::all();
        return view('print.list_print', compact('prints'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('print.create_print');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $print = new \App\Printer;
        $print->model=$request->get('model');
        $print->brand=$request->get('brand');
        $print->ip=$request->get('ip');
        $print->sector=$request->get('sector');
        $print->save();
        $printId = $print->id;
        $counters = new \App\Counter;
        $counters->id_print=$printId;
        $counters->date=$date = date('Y-m-d H:i');
        $counters->counters = 0;
        $counters->total_page=$request->get('first_value');
        $counters->save();

        return redirect('/print');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prints= \App\Printer::find($id);
        $counter= \App\Counter::where('id_print',$id)->orderBy('id','desc')->first()->total_page;        
        return view('print.edit_print',compact('prints','id','counter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $print= \App\Printer::find($id)->with('counter')->first();        
        $print->model=$request->get('model');
        $print->brand=$request->get('brand');
        $print->ip=$request->get('ip');
        $print->sector=$request->get('sector');
        $print->save();
        $counter= new \App\Counter();
        $counter->date=$date = date('Y-m-d H:i');
        $counter->id_print=$print->id;
        $counter->counters = 0;
        $counter->total_page=$request->get('first_value');
        $counter->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $prints = \App\Printer::find($id);
            $prints->delete();
            return redirect('print');
        }catch (\PDOException $e) {
            return redirect()->back()->with('error', 'Essa impressora faz parte de um contrato ativo');
        }
    
       
    }
    public function teste()
    {
        return view('testebootstrap');
    }
}
